#!/usr/bin/env python
# encoding: utf-8
class IPMCDevAMC:

    GND = 0
    VCC = 1

    def __init__(self, IPMCDevComObj):
        self.IPMCDevComObj = IPMCDevComObj

    def SETGA(self, ha):
        str = self.IPMCDevComObj.query('SETAMCGA 0x{:02X}'.format(ha))
        if 'Set AMC Geographical address' in str:
            if '{:02X}'.format(ha) in str:
                return 0
            else:
                return -1
        else:
            return -2

    def SELAMC(self, port):
        if port < 0 or port > 8:
            return -1

        str = self.IPMCDevComObj.query('SELAMC 0x{:02X}'.format(port))
        if 'Select AMC slot' in str:
            if int(str[str.find("(")+1:str.find(")")]) == port:
                return 0
            else:
                return -3
        else:
            return -2

    def INSERTAMC(self):
        str = self.IPMCDevComObj.query('SETPS1')
        if 'Set AMC PS1# to GND' in str:
            return 0
        else:
            return -2

    def EXTRACTAMC(self):
        str = self.IPMCDevComObj.query('CLRPS1')
        if 'Set AMC PS1# to VCC' in str:
            return 0
        else:
            return -2

    def CLOSEHSW(self):
        str = self.IPMCDevComObj.query('CLOSEAMCHS')
        if 'AMC Handle switch closed' in str:
            return 0
        else:
            return -2

    def OPENHSW(self):
        str = self.IPMCDevComObj.query('OPENAMCHS')
        if 'AMC Handle switch opened' in str:
            return 0
        else:
            return -2

    def GETMPEN(self):
        str = self.IPMCDevComObj.query('GETAMCMPEN')

        if 'MP Enable' in str:
            if 'OFF' in str.split(':')[1]:
                return 0
            elif 'ON' in str.split(':')[1]:
                return 1
            else:
                return -3

        else:
            return -2

    def GETPPEN(self):
        str = self.IPMCDevComObj.query('GETAMCPPEN')

        if 'PP Enable' in str:
            if 'OFF' in str.split(':')[1]:
                return 0
            elif 'ON' in str.split(':')[1]:
                return 1
            else:
                return -3

        else:
            return -2

    def ASSERTMPGOOD(self):
        str = self.IPMCDevComObj.query('ASSERTMPGOOD')
        if 'Assert Management power good' in str:
            return 0
        else:
            return -2

    def ASSERTPPGOOD(self):
        str = self.IPMCDevComObj.query('ASSERTPPGOOD')
        if 'Assert Payload power good' in str:
            return 0
        else:
            return -2

    def DEASSERTMPGOOD(self):
        str = self.IPMCDevComObj.query('DEASSERTMPGOOD')
        if 'De-assert Management power good' in str:
            return 0
        else:
            return -2

    def DEASSERTPPGOOD(self):
        str = self.IPMCDevComObj.query('DEASSERTPPGOOD')
        if 'De-assert Payload power good' in str:
            return 0
        else:
            return -2