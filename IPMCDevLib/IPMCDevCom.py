#!/usr/bin/env python
# encoding: utf-8
import serial
import sys
import io
import glob

class IPMCDevCom:

    def __init__(self, **kwargs):

        self.ser = None

        if kwargs.get('umgt_port') is None:
            print('[Info] searching for uMGT on serial ports ...')
            ports = self.listPorts()
        else:
            print(f'[Info] using serial port {kwargs.get("umgt_port")} to communicate with uMGT')
            ports = [ kwargs.get('umgt_port') ]

        self.debug = 0
        if not kwargs.get('debug') is None:
            self.debug = kwargs.get('debug')

        # This loop sends the string "GETDEVICE" out on all serial ports listed. The uMGT responds to this command with IPMCTP_CTRLER.
        # See ipmc-tester/setup-config/umgt-firmware/main.c
        for port in ports:
            with serial.Serial(port, 9600, timeout=1, write_timeout=1) as ser:
                try:
                    ser.write("GETDEVICE\n".encode())
                    hello = ser.readline()
                    if 'IPMCTP_CTRLER' in hello.decode():
                        self.ser = ser
                        break
                except:
                    continue

        if self.ser == None:
            raise ValueError('No uMGT serial port found (UART connection cannot be established - Warning: please ensure that the port is not already used by another program).')
        else:
            print('[Info] uMGT serial port found at \"{}\"'.format(port))

    def listPorts(self):
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def send(self, command):
        if self.ser.isOpen() != True:
            self.ser.open()

        self.ser.write("{}\n".format(command).encode())
        self.ser.flush()

    def query(self, command):
        if self.ser.isOpen() != True:
            self.ser.open()

        self.ser.reset_input_buffer()
        self.ser.reset_output_buffer()

        self.ser.write("{}\n".format(command).encode())
        self.ser.flush()
        reply = self.ser.readline().decode().rstrip()
        if self.debug:
            print(f'[Debug] uMGT reply: {reply}')
        return reply
        #return self.ser.readline().decode().rstrip()

    def close(self):
        self.ser.close()

