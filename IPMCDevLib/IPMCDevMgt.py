#!/usr/bin/env python
# encoding: utf-8

class IPMCDevMgt:

    IPMBABus = 0x00
    IPMBBBus = 0x01
    IPMBLBus = 0x02
    IPMCSensBus = 0x03
    IPMCMgtBus = 0x04

    def __init__(self, IPMCDevComObj):
        self.IPMCDevComObj = IPMCDevComObj

    def GETMGTVOLTAGE(self):
        str = self.IPMCDevComObj.query('GETMGTVOLTAGE')
        return 305.18e-6 * float(str.split(':')[1])

    def GETIPMCVOLTAGE(self):
        str = self.IPMCDevComObj.query('GETIPMCVOLTAGE')
        return 305.18e-6 * float(str.split(':')[1])

    def GETIPMCCURRENT(self):
        str = self.IPMCDevComObj.query('GETIPMCCURRENT')
        r_sense = 0.1
        return 19.42e-6 * float(str.split(':')[1]) / r_sense

    def SELI2C(self, id):
        checkArr = [self.IPMBABus, self.IPMBBBus, self.IPMBLBus, self.IPMCSensBus, self.IPMCMgtBus]

        if id in checkArr:
            str = self.IPMCDevComObj.query('SELI2C 0x{:02X}'.format(id))
            if 'Select I2C port' in str:
                return 0
            else:
                return -2

        else:
            return -1
