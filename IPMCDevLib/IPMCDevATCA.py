# encoding: utf-8
class IPMCDevATCA:

    GND = 0
    VCC = 1

    def __init__(self, IPMCDevComObj):
        self.IPMCDevComObj = IPMCDevComObj

    def SETHA(self, ha):
        str = self.IPMCDevComObj.query('SETHA 0x{:02X}'.format(ha))
        if 'Set IPMC Hardware' in str:
            if f'{ha:02x}' in str:
                return 0
            else:
                return -1
        else:
            return -2

    def GETHA(self):
        str = self.IPMCDevComObj.query('GETHA')
        hw_addr = str.split(':')[1]
        return hw_addr

    def CONFHSW(self, state):
        if state == self.GND:
            str = self.IPMCDevComObj.query('CLRHSW')
            if 'Clear Handle switch' in str:
                return 0
            else:
                return -2

        elif state == self.VCC:
            str = self.IPMCDevComObj.query('SETHSW')
            if 'Set Handle switch' in str:
                return 0
            else:
                return -2

        else:
            return -1

    def SETIO(self, id, state):
        if state == self.GND:
            str = self.IPMCDevComObj.query('CLRIO 0x{:02X}'.format(id))
            if 'Clear IO' in str:
                if int(str[str.find("(")+1:str.find(")")]) == id:
                    return 0
                else:
                    return -3
            else:
                return -2

        elif state == self.VCC:
            str = self.IPMCDevComObj.query('SETIO 0x{:02X}'.format(id))
            if 'Set IO' in str:
                if int(str[str.find("(")+1:str.find(")")]) == id:
                    return 0
                else:
                    return -3
            else:
                return -2

        else:
            return -1

    def GETIO(self, id):
        str = self.IPMCDevComObj.query('GETIO 0x{:02X}'.format(id))

        if 'Pin state' in str:
            if int(str[str.find("(")+1:str.find(")")]) == id:
                if '01' in str.split(':')[1]:
                    return self.VCC
                elif '00' in str.split(':')[1]:
                    return self.GND
                else:
                    return -4 #Pin value error

            else:
                return -3 #Pin id error

        else:
            return -2 #Wrong command

    def GETBLUELED(self):
        str = self.IPMCDevComObj.query('GETBLUELED')

        if 'Blue led' in str:
            if 'OFF' in str.split(':')[1]:
                return 0
            elif 'ON' in str.split(':')[1]:
                return 1
            else:
                return -3

        else:
            return -2

    def GETLED1(self):
        str = self.IPMCDevComObj.query('GETLED1')

        if 'FRU LED 1' in str:
            if 'OFF' in str.split(':')[1]:
                return 0
            elif 'ON' in str.split(':')[1]:
                return 1
            else:
                return -3

        else:
            return -2

    def GETLED2(self):
        str = self.IPMCDevComObj.query('GETLED2')

        if 'FRU LED 2' in str:
            if 'OFF' in str.split(':')[1]:
                return 0
            elif 'ON' in str.split(':')[1]:
                return 1
            else:
                return -3

        else:
            return -2

    def GETLED3(self):
        str = self.IPMCDevComObj.query('GETLED3')

        if 'FRU LED 3' in str:
            if 'OFF' in str.split(':')[1]:
                return 0
            elif 'ON' in str.split(':')[1]:
                return 1
            else:
                return -3

        else:
            return -2

    def GETLED(self, led):
       if led == 0:
           return self.GETBLUELED()
       elif led == 1:
           return self.GETLED1()
       elif led == 2:
           return self.GETLED2()
       elif led == 3:
           return self.GETLED3()
       else:
           raise Exception(f'invalid argument led = {led}')

    def GET12VEN(self):
        str = self.IPMCDevComObj.query('GET12VEN')
        if '12V Enable' in str:
            state = str.split(':')[1]
            if 'OFF' in state:
                return 0
            elif 'ON' in state:
                return 1
            else:
                print(f'[Error] GET12VEN - unknown state {state} received')
                return -3

        else:
            print(f'[Error] GET12VEN - wrong reply from uMGT ({str})')
            return -2

    def ASSERTPGOODA(self):
        str = self.IPMCDevComObj.query('ASSERTPGOODA')
        if 'Unknown command' in str:
            return -2
        else:
            return 0

    def ASSERTPGOODB(self):
        str = self.IPMCDevComObj.query('ASSERTPGOODB')
        if 'Unknown command' in str:
            return -2
        else:
            return 0

    def DEASSERTPGOODA(self):
        str = self.IPMCDevComObj.query('DEASSERTPGOODA')
        if 'Unknown command' in str:
            return -2
        else:
            return 0

    def DEASSERTPGOODB(self):
        str = self.IPMCDevComObj.query('DEASSERTPGOODB')
        if 'Unknown command' in str:
            return -2
        else:
            return 0
