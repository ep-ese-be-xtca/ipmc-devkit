# CERN-IPMC DevKit

The complete CERN-IPMC development kit is made of a custom board used to emulate an AdvancedTCA Carrier blade 
associated to PigeonPoint’s Shelf Manager development kit (not included, to be procured from PigeonPoint 
– ref. KIT BTC 7200R ATCA) used to simulate a crate environment. Nevertheless, the  IPMC tester electronics 
card hosting the CERN-IPMC can also be used as a standalone environment to test all of the system independent 
features of an IPMC module (e.g.: JTAGs, I/Os, Sensor drivers, etc.). 

The system is supervised by an 8bit microcontroller managed by a PC via a USB to serial interface. 
A Python library was designed to execute commands for the monitoring and control of the IPMC interfaces

IPMC DevKit documenation: <a href='https://espace.cern.ch/ph-dep-ESE-BE-ATCAEvaluationProject/PP_IPMC/Public%20documents/CERN-IPMC%20DevKit.pdf'>SharePoint</a>

# How-To

This python library was designed using python 2.7.14

## Dependencies

List of dependency(ies): 
- <b>pySerial</b> (installation command: pip install pyserial)
