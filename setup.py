from setuptools import setup, find_packages

setup(
    name="IPMCDevLib",
    version="1.0",
    packages=find_packages(),
    python_requires='>=3.4',

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=["pyserial"],

    # metadata to display on PyPI
    author="Julian Mendez",
    author_email="julian.mendez@cern.ch",
    description="IPMC Devkit controller package",
    keywords="CERN-IPMC",
    url="http://cern-ipmc.web.cern.ch/",   # project home page, if any
    classifiers=[
        "License :: OSI Approved :: Python Software Foundation License"
    ]

    # could also include long_description, download_url, etc.
)