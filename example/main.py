#!/usr/bin/env python
# encoding: utf-8
from IPMCDevLib.IPMCDevCom import IPMCDevCom
from IPMCDevLib.IPMCDevMgt import IPMCDevMgt
from IPMCDevLib.IPMCDevATCA import IPMCDevATCA
from IPMCDevLib.IPMCDevAMC import IPMCDevAMC

if __name__ == '__main__':
    IPMCDevObject = IPMCDevCom()
    
    #+++++++++++++++++++++++++
    #+ IPMC Tester functions +
    #+++++++++++++++++++++++++    
    IPMCDevMgtObject = IPMCDevMgt(IPMCDevObject)
    
    print("Tester voltage: {} Volts".format(IPMCDevMgtObject.GETMGTVOLTAGE()))
    print("IPMC Voltage: {} Volts".format(IPMCDevMgtObject.GETIPMCVOLTAGE()))
    print("IPMC Current: {} Amps".format(IPMCDevMgtObject.GETIPMCCURRENT()))
    
    selI2c = IPMCDevMgtObject.SELI2C(IPMCDevMgtObject.IPMBABus)
    if selI2c < 0:
        print("I2C Selection issue: {}".format(selI2c))
        
    #+++++++++++++++++++++++++++
    #+  ATCA related functions +
    #+++++++++++++++++++++++++++    
    IPMCDevATCAObject = IPMCDevATCA(IPMCDevObject)                  # Create IPMCDevATCA instance
    
    # Hardware address:
    #------------------
    setha = IPMCDevATCAObject.SETHA(0x43)                           # Set IPMC Hardware Address (0x41)
    if setha < 0:                                                   # Check command success
        print("HA Setting issue: {}".format(setha))
    
    # Handle switch:
    #---------------
    sethw = IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.GND)        # Set Handle switch value to GND
    if sethw < 0:                                                   # Check command success
        print("Handle switch issue: {}".format(sethw))
        
    sethw = IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)        # Set Handle switch value to VCC
    if sethw < 0:                                                   # Check command success
        print("Handle switch issue: {}".format(sethw))
     
    # I/Os:
    #------
    setio = IPMCDevATCAObject.SETIO(0, IPMCDevATCAObject.VCC)       # Set I/O pin 0 to VCC
    if setio < 0:                                                   # Check command success
        print("Set IO issue: {}".format(setio))
        
    setio = IPMCDevATCAObject.SETIO(0x0F, IPMCDevATCAObject.GND)    # Set I/O pin 15 to GND
    if setio < 0:                                                   # Check command success
        print("Set IO issue: {}".format(setio))
    
    getio = IPMCDevATCAObject.GETIO(0x0F)                           # Get I/O pin 15 state
    if getio < 0:                                                   # Check command success
        print("Get IO issue: {}".format(getio))
    elif getio == IPMCDevATCAObject.GND:
        print('IO (0x0F): GND')
    elif getio == IPMCDevATCAObject.VCC:
        print('IO (0x0F): VCC')
        
    # Blue led:
    #----------
    blueled = IPMCDevATCAObject.GETBLUELED()                        # Get Blue led state
    if blueled < 0:                                                 # Check command success
        print("Get blue led issue: {}".format(blueled))
    elif blueled == 0:
        print("Blue led is OFF")
    elif blueled == 1:
        print("Blue led is ON")
        
    # 12V En.:
    #---------
    ppen = IPMCDevATCAObject.GET12VEN()                             # Get 12V Enable state
    if ppen < 0:                                                    # Check command success
        print("Get 12V En. issue: {}".format(ppen))
    elif ppen == 0:
        print("12V Enable is OFF")
    elif ppen == 1:
        print("12V Enable is ON")
        
    # PGOOD Assertion:
    #-----------------
    assertPgA = IPMCDevATCAObject.ASSERTPGOODA()                    # Assert IPMC's PGoodA pin
    if assertPgA < 0:                                               # Check command success
        print("Assert PGOOD A issue: {}".format(assertPgA))
        
    assertPgB = IPMCDevATCAObject.ASSERTPGOODB()                    # Assert IPMC's PGoodB pin
    if assertPgB < 0:                                               # Check command success
        print("Assert PGOOD A issue: {}".format(assertPgB))
        
    deassertPgA = IPMCDevATCAObject.DEASSERTPGOODA()                # De-assert IPMC's PGoodA pin
    if deassertPgA < 0:                                             # Check command success
        print("Assert PGOOD A issue: {}".format(deassertPgA))
        
    deassertPgB = IPMCDevATCAObject.DEASSERTPGOODB()                # De-assert IPMC's PGoodA pin
    if deassertPgB < 0:                                             # Check command success
        print("Assert PGOOD A issue: {}".format(deassertPgB))
        
    #++++++++++++++++++++++++++
    #+  AMC related functions +
    #++++++++++++++++++++++++++
    IPMCDevAMCObject = IPMCDevAMC(IPMCDevObject)                    # Create IPMCDevAMC instance
    
    setga = IPMCDevAMCObject.SETGA(0x72)                            # Set AMC Geaographical address
    if setga < 0:
        print("Set AMC GA issue: {}".format(setga))
    
    selamc = IPMCDevAMCObject.SELAMC(0)                             # Select IPMC port to connect to the emulated AMC
    if selamc < 0:
        print("Select AMC port issue: {}".format(selamc))
        
    insamc = IPMCDevAMCObject.INSERTAMC()                           # Emulate AMC insertion
    if insamc < 0:
        print("Insert AMC issue: {}".format(insamc))
        
    extamc = IPMCDevAMCObject.EXTRACTAMC()                          # Emulate AMC extraction
    if extamc < 0:
        print("Extract AMC issue: {}".format(extamc))
        
    closehsw = IPMCDevAMCObject.CLOSEHSW()                          # Close AMC handle switch
    if closehsw < 0:
        print("Close AMC's handle switch issue: {}".format(closehsw))
        
    openhsw = IPMCDevAMCObject.OPENHSW()                            # Open AMC handle switch
    if openhsw < 0:
        print("Open AMC handle switch issue: {}".format(openhsw))
        
    mpen = IPMCDevAMCObject.GETMPEN()                               # Get AMC's Management power state
    if mpen < 0:
        print("Get MP Enable issue: {}".format(mpen))
    elif mpen == 0:
        print("AMC MP: OFF");
    elif mpen == 1:
        print("AMC MP: ON");
        
    ppen = IPMCDevAMCObject.GETPPEN()                               # Get AMC's payload power state
    if ppen < 0:
        print("Get PP Enable issue: {}".format(ppen))
    elif mpen == 0:
        print("AMC PP: OFF");
    elif mpen == 1:
        print("AMC PP: ON");
        
    assmpgood = IPMCDevAMCObject.ASSERTMPGOOD()                     # Assert MP good signal
    if assmpgood < 0:
        print("Assert MP Good issue: {}".format(assmpgood))
        
    assppgood = IPMCDevAMCObject.ASSERTPPGOOD()                     # Assert PP good signal
    if assppgood < 0:
        print("Assert PP Good issue: {}".format(assppgood))
        
    deassmpgood = IPMCDevAMCObject.DEASSERTMPGOOD()                 # De-assert MP good signal
    if deassmpgood < 0:
        print("De-assert MP Good issue: {}".format(deassmpgood))
        
    deassppgood = IPMCDevAMCObject.DEASSERTPPGOOD()                 # De-assert PP good signal
    if deassppgood < 0:
        print("De-assert PP good issue: {}".format(deassppgood))